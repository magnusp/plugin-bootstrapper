<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="">Name</label>\
    <div class="controls">\
        <input{if !empty($frontendLink->Name)} value="{$frontendLink->Name}" {/if} type="text" name="jtlshop3plugin-FrontendLink-Link-Name[]" class="form-control required" placeholder="Meine tolle Seite">\
    </div>\
    <label class="control-label" for="">Dateiname</label>\
    <div class="controls">\
        <input{if !empty($frontendLink->Filename)} value="{$frontendLink->Filename}" {/if} type="text" name="jtlshop3plugin-FrontendLink-Link-Filename[]" class="form-control required" placeholder="my_page.php">\
    </div>\
    <label class="control-label" for="">Template (leer lassen, wenn FullScreenTemplate genutzt werden soll)</label>\
    <div class="controls">\
        <input{if !empty($frontendLink->Template)} value="{$frontendLink->Template}" {/if} type="text" name="jtlshop3plugin-FrontendLink-Link-Template[]" class="form-control" placeholder="my_page.tpl">\
    </div>\
    <label class="control-label" for="">FullscreenTemplate? (leer lassen, wenn nicht gew&uuml;nscht)</label>\
    <div class="controls">\
        <input{if !empty($frontendLink->FullscreenTemplate)} value="{$frontendLink->FullscreenTemplate}" {/if} type="text" name="jtlshop3plugin-FrontendLink-Link-FullscreenTemplate[]" class="form-control" placeholder="my_fullscreen_page.tpl">\
    </div>\
    <label class="control-label" for="">Nur sichtbar noch Login?</label>\
    <div class="controls">\
        <select name="jtlshop3plugin-FrontendLink-Link-VisibleAfterLogin[]" class="form-control">\
            <option{if !empty($frontendLink->VisibleAfterLogin) && $frontendLink->VisibleAfterLogin === 'Y'} selected{/if} value="Y">Ja</option>\
            <option{if empty($frontendLink->VisibleAfterLogin) || $frontendLink->VisibleAfterLogin === 'N'} selected{/if} value="N">Nein</option>\
        </select>\
    </div>\
    <label class="control-label" for="">Drucken-Button?</label>\
    <div class="controls">\
        <select name="jtlshop3plugin-FrontendLink-Link-PrintButton[]" class="form-control">\
            <option{if !empty($frontendLink->PrintButton) && $frontendLink->PrintButton === 'Y'} selected{/if} value="Y">Ja</option>\
            <option{if empty($frontendLink->PrintButton) || $frontendLink->PrintButton === 'N'} selected{/if} value="N">Nein</option>\
        </select>\
    </div>\
    <label class="control-label" for="">SSL-Verschl&uuml;sselung</label>\
    <div class="controls">\
        <select name="jtlshop3plugin-FrontendLink-Link-SSL[]" class="form-control">\
            <option{if !isset($frontendLink->SSL) || $frontendLink->SSL === 0} selected{/if} value="0">Standard</option>\
            <option{if !empty($frontendLink->SSL) && $frontendLink->SSL === 2} selected{/if} value="2">erzwungen</option>\
        </select>\
    </div>\
    <label class="control-label" for="">Linkgruppe</label>\
    <div class="controls">\
        <input{if !empty($frontendLink->LinkGroup)} value="{$frontendLink->LinkGroup}" {/if} type="text" name="jtlshop3plugin-FrontendLink-Link-LinkGroup[]" class="form-control required" placeholder="hidden">\
    </div>\
    <label class="control-label" for="">Seo-URL</label>\
    <div class="controls">\
        <input{if !empty($frontendLink->LinkLanguage[0]->Seo)} value="{$frontendLink->LinkLanguage[0]->Seo}" {/if} type="text" name="jtlshop3plugin-FrontendLink-Link-LinkLanguage-Seo[]" class="form-control required" placeholder="Meine-tolle-Seite">\
    </div>\
    <label class="control-label" for="">Link-Name</label>\
    <div class="controls">\
        <input{if !empty($frontendLink->LinkLanguage[0]->Name)} value="{$frontendLink->LinkLanguage[0]->Name}" {/if} type="text" name="jtlshop3plugin-FrontendLink-Link-LinkLanguage-Name[]" class="form-control required" placeholder="MeineTolleSeite">\
    </div>\
    <label class="control-label" for="">Titel</label>\
    <div class="controls">\
        <input{if !empty($frontendLink->LinkLanguage[0]->Title)} value="{$frontendLink->LinkLanguage[0]->Title}" {/if} type="text" name="jtlshop3plugin-FrontendLink-Link-LinkLanguage-Title[]" class="form-control required" placeholder="Meine Tolle Seite">\
    </div>\
    <label class="control-label" for="">Meta Title</label>\
    <div class="controls">\
        <input{if !empty($frontendLink->LinkLanguage[0]->MetaTitle)} value="{$frontendLink->LinkLanguage[0]->MetaTitle}" {/if} type="text" name="jtlshop3plugin-FrontendLink-Link-LinkLanguage-MetaTitle[]" class="form-control required" placeholder="Meine Seite">\
    </div>\
    <label class="control-label" for="">Meta Keywords</label>\
    <div class="controls">\
        <input{if !empty($frontendLink->LinkLanguage[0]->MetaKeywords)} value="{$frontendLink->LinkLanguage[0]->MetaKeywords}" {/if} type="text" name="jtlshop3plugin-FrontendLink-Link-LinkLanguage-MetaKeywords[]" class="form-control required" placeholder="Meine,Tolle,Seite">\
    </div>\
    <label class="control-label" for="">Meta Description</label>\
    <div class="controls">\
        <input{if !empty($frontendLink->LinkLanguage[0]->MetaDescription)} value="{$frontendLink->LinkLanguage[0]->MetaDescription}" {/if} type="text" name="jtlshop3plugin-FrontendLink-Link-LinkLanguage-MetaDescription[]" class="form-control required" placeholder="Meine Tolle Seite ist eine tolle Seite">\
    </div>\
    <hr>\
</div>\
