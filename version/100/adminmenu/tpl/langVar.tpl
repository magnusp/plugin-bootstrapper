<div class="control-group">\
    <a class="remove-group" href="#"><i class="fa fa-remove"></i></a>\
    <label class="control-label" for="lang-var-name">Name</label>\
    <div class="controls">\
        <input{if !empty($langVar->Name)} value="{$langVar->Name}" {/if} type="text" name="jtlshop3plugin-Locales-Variable-Name[]" class="form-control required" placeholder="my_lang_var_x">\
    </div>\
    <label class="control-label" for="lang-var-name">Beschreibung</label>\
    <div class="controls">\
        <input{if !empty($langVar->Description)} value="{$langVar->Description}" {/if} type="text" name="jtlshop3plugin-Locales-Variable-Description[]" class="form-control required" placeholder="Mein Beschreibung">\
    </div>\
    <label class="control-label" for="name">Wert (Deutsch)</label>\
    <div class="controls">\
        <input{if !empty($langVar->Values.GER)} value="{$langVar->Values.GER}" {/if} type="text" name="jtlshop3plugin-Locales-Variable.GER[]" class="form-control required" placeholder="Deutsche &Uuml;bersetzung">\
    </div>\
    <label class="control-label" for="name">Wert (Englisch)</label>\
    <div class="controls">\
        <input{if !empty($langVar->Values.ENG)} value="{$langVar->Values.ENG}" {/if} type="text" name="jtlshop3plugin-Locales-Variable.ENG[]" class="form-control required" placeholder="English translation">\
    </div>\
    <hr>\
</div>\
